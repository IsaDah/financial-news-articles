# Financial_NewsArticles
An App written in Java that makes use of NewsAPI.org it currently only supports CNBC, Business Insider, Crypto Coins News and Financial Post, will add more in the future, built it to test NewsAPI.org

The app obviously needs internet connection to retrieve the articles.

The App is powered by NewsAPI.org
https://newsapi.org/

My app is based on this tutorial
https://androstock.com/tutorials/create-a-news-app-on-android-android-studio.html
Modifications include changes to the user interface as well as adding more news outlets.
