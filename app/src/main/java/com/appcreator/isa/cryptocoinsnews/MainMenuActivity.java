package com.appcreator.isa.cryptocoinsnews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends AppCompatActivity
{

    private Button CNBC_Button;
    private Button Business_Insider_Button;
    private Button Financial_Post_Button;
    private Button Crypto_Coins_News_Button;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        CNBC_Button = (Button)findViewById(R.id.button_cnbc);
        Business_Insider_Button = (Button)findViewById(R.id.button_business_insider);
        Financial_Post_Button = (Button)findViewById(R.id.button_financial_post);
        Crypto_Coins_News_Button = (Button)findViewById(R.id.button_crypto_coins_news);
    }

    public void goToCNBC(View view)
    {
        CNBC_Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(MainMenuActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void goToBusinessInsider(View view)
    {
        Business_Insider_Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(MainMenuActivity.this, BusinessInsiderActivity.class);
                startActivity(i);
            }
        });
    }


    public void goToFinancialPost(View view)
    {
        Financial_Post_Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(MainMenuActivity.this, FinancialPostActivity.class);
                startActivity(i);
            }
        });
    }

    public void goToCryptoCoinsNews(View view)
    {
        Crypto_Coins_News_Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(MainMenuActivity.this, CryptoCoinsNewsActivity.class);
                startActivity(i);
            }
        });
    }

}
