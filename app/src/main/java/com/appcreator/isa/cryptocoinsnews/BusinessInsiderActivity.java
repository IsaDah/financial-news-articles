package com.appcreator.isa.cryptocoinsnews;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class BusinessInsiderActivity extends AppCompatActivity {
    String API_KEY = "bc1ef62ddf97409ca681d45bcc6ad38a";
    String NEWS_SOURCE = "business-insider";
    ListView financeNews;
    ProgressBar loader;

    ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();

    static final String KEY_AUTHOR = "author";
    static final String KEY_TITLE = "title";
    static final String KEY_DESCRIPTION = "description";
    static final String KEY_URL = "url";
    static final String KEY_URLTOIMAGE = "urlToImage";
    static final String KEY_PUBLISH_DATE = "publishedAt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_insider);


        financeNews = (ListView) findViewById(R.id.financeNews);
        loader = (ProgressBar) findViewById(R.id.loaderFinanceNews);
        financeNews.setEmptyView(loader);

        if (FunctionNetwork.isNetworkThere(getApplicationContext())) {
            BusinessInsiderActivity.GrabCryptoNews newsTask = new BusinessInsiderActivity.GrabCryptoNews();
            newsTask.execute();
        } else {
            Toast.makeText(getApplicationContext(), "Couldn't not Connect, Check your connection !", Toast.LENGTH_LONG).show();
        }
    }

    class GrabCryptoNews extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            String xml = "";

            String urlParameters = "";
            xml = FunctionNetwork.executeGet("https://newsapi.org/v2/top-headlines?sources=" + NEWS_SOURCE + "&apiKey=" + API_KEY, urlParameters);
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {
            if (xml.length() > 10) {
                try {
                    JSONObject jsonResponse = new JSONObject(xml);
                    JSONArray jsonArray = jsonResponse.optJSONArray("articles");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(KEY_AUTHOR, jsonObject.optString(KEY_AUTHOR).toString());
                        map.put(KEY_TITLE, jsonObject.optString(KEY_TITLE).toString());
                        map.put(KEY_DESCRIPTION, jsonObject.optString(KEY_DESCRIPTION));
                        map.put(KEY_URL, jsonObject.optString(KEY_URL).toString());
                        map.put(KEY_URLTOIMAGE, jsonObject.optString(KEY_URLTOIMAGE).toString());
                        map.put(KEY_PUBLISH_DATE, jsonObject.optString(KEY_PUBLISH_DATE).toString());
                        dataList.add(map);
                    }

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Something wrong...", Toast.LENGTH_SHORT).show();
                }

                ListNewsAdapter adapter = new ListNewsAdapter(BusinessInsiderActivity.this, dataList);
                financeNews.setAdapter(adapter);

                financeNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(BusinessInsiderActivity.this, DetailsActivity.class);
                        i.putExtra("url", dataList.get(+position).get(KEY_URL));
                        startActivity(i);
                    }
                });

            } else {
                Toast.makeText(getApplicationContext(), "No news found", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
