package com.appcreator.isa.cryptocoinsnews;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


class ListNewsAdapter extends BaseAdapter
{
    private Activity activity;
    private ArrayList<HashMap<String,String>> data;

    public ListNewsAdapter(Activity a, ArrayList<HashMap<String, String>> d)
    {
        activity = a;
        data = d;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    public Object getItem(int position)
    {
        return position;
    }

    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ListNewsViewHolder holder = null;
        if(convertView == null)
        {
            holder = new ListNewsViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.list_row, parent,false);
            holder.galleryImage = (ImageView)convertView.findViewById(R.id.financeNewsImage);
            holder.author = (TextView) convertView.findViewById(R.id.source);
            holder.newsTitle = (TextView) convertView.findViewById(R.id.financeNewsTitle);
            holder.newsDetail = (TextView) convertView.findViewById(R.id.financeNewsDetails);
            holder.date = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        }

        else
        {
            holder = (ListNewsViewHolder) convertView.getTag();
        }

        holder.galleryImage.setId(position);
        holder.author.setId(position);
        holder.newsTitle.setId(position);
        holder.newsDetail.setId(position);
        holder.date.setId(position);

        HashMap<String, String> imgNews = new HashMap<String, String>();
        imgNews = data.get(position);

        try
        {
            holder.author.setText(imgNews.get(MainActivity.KEY_AUTHOR));
            holder.newsTitle.setText(imgNews.get(MainActivity.KEY_TITLE));
            holder.date.setText(imgNews.get(MainActivity.KEY_PUBLISH_DATE));
            holder.newsDetail.setText(imgNews.get(MainActivity.KEY_DESCRIPTION));

            if(imgNews.get(MainActivity.KEY_URLTOIMAGE).toString().length() < 5)
            {
                holder.galleryImage.setVisibility(View.GONE);
            }
            else
            {
                Picasso.with(activity).load(imgNews.get(MainActivity.KEY_URLTOIMAGE).toString()).resize(300, 200).into(holder.galleryImage);
            }
        }

        catch(Exception e) 
        {

        }

        try
        {
            holder.author.setText(imgNews.get(BusinessInsiderActivity.KEY_AUTHOR));
            holder.newsTitle.setText(imgNews.get(BusinessInsiderActivity.KEY_TITLE));
            holder.date.setText(imgNews.get(BusinessInsiderActivity.KEY_PUBLISH_DATE));
            holder.newsDetail.setText(imgNews.get(BusinessInsiderActivity.KEY_DESCRIPTION));

            if(imgNews.get(BusinessInsiderActivity.KEY_URLTOIMAGE).toString().length() < 5)
            {
                holder.galleryImage.setVisibility(View.GONE);
            }
            else
            {
                Picasso.with(activity).load(imgNews.get(BusinessInsiderActivity.KEY_URLTOIMAGE).toString()).resize(300, 200).into(holder.galleryImage);
            }
        }

        catch(Exception e)
        {

        }

        try
        {
            holder.author.setText(imgNews.get(FinancialPostActivity.KEY_AUTHOR));
            holder.newsTitle.setText(imgNews.get(FinancialPostActivity.KEY_TITLE));
            holder.date.setText(imgNews.get(FinancialPostActivity.KEY_PUBLISH_DATE));
            holder.newsDetail.setText(imgNews.get(FinancialPostActivity.KEY_DESCRIPTION));

            if(imgNews.get(FinancialPostActivity.KEY_URLTOIMAGE).toString().length() < 5)
            {
                holder.galleryImage.setVisibility(View.GONE);
            }
            else
            {
                Picasso.with(activity).load(imgNews.get(FinancialPostActivity.KEY_URLTOIMAGE).toString()).resize(300, 200).into(holder.galleryImage);
            }
        }

        catch(Exception e)
        {

        }

        try
        {
            holder.author.setText(imgNews.get(CryptoCoinsNewsActivity.KEY_AUTHOR));
            holder.newsTitle.setText(imgNews.get(CryptoCoinsNewsActivity.KEY_TITLE));
            holder.date.setText(imgNews.get(CryptoCoinsNewsActivity.KEY_PUBLISH_DATE));
            holder.newsDetail.setText(imgNews.get(CryptoCoinsNewsActivity.KEY_DESCRIPTION));

            if(imgNews.get(CryptoCoinsNewsActivity.KEY_URLTOIMAGE).toString().length() < 5)
            {
                holder.galleryImage.setVisibility(View.GONE);
            }
            else
            {
                Picasso.with(activity).load(imgNews.get(CryptoCoinsNewsActivity.KEY_URLTOIMAGE).toString()).resize(300, 200).into(holder.galleryImage);
            }
        }

        catch(Exception e)
        {

        }

        return convertView;
    }
}



class ListNewsViewHolder
{
    ImageView galleryImage;
    
    TextView author;
    TextView newsTitle;
    TextView newsDetail;
    TextView date;
}
